package INF101.lab2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    final int max_size = 20;
    List<FridgeItem> items;

    public Fridge() {
        items = new ArrayList<FridgeItem>();
    }

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(nItemsInFridge() < totalSize()) {
            items.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) throws NoSuchElementException {
        if(!items.remove(item)) {
            throw new NoSuchElementException();
        }       
    }

    @Override
    public void emptyFridge() {
        items.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired_foods = new ArrayList<FridgeItem>();
        Iterator<FridgeItem> iter = items.iterator();
        FridgeItem item;
        while(iter.hasNext()) {
            item = iter.next();
            if(item.hasExpired()) {
                expired_foods.add(item);
                iter.remove();
            }
        }
        return expired_foods;
    }
}
